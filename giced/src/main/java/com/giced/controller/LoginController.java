package com.giced.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.giced.model.Users;
import com.giced.service.UsersService;

@Controller
public class LoginController {
	
	  @Autowired
	  UsersService userService;
	  
	@Autowired 
	public UsersService getUserService() {
		return userService;
	}

	@Autowired
	public void setUserService(UsersService userService) {
		this.userService = userService;
	}


	//Login Process 
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public String login(@ModelAttribute("user") Users user,Model model) {
		List<Users> listUser = userService.validateUser(user.getUser_name(),user.getUser_password());
		Users loginUser=new Users();
		if(listUser.size()>0) {
			loginUser=listUser.get(0);
		}
		String msg="";
		if(null!=loginUser.getUser_name()) {
			if(loginUser.getRole_id().equals("super-admin")){
				model.addAttribute("user_id", loginUser.getUser_id());
				model.addAttribute("user_name", loginUser.getUser_name());
				msg="Admin_Login";
			}
			else {
				msg="Login";
				model.addAttribute("errorMsg", "Unauthorised User!");
			}				
		}
		else {
		msg="Login";
		model.addAttribute("errorMsg", "Incorrect Credentials!");
		}
		return msg;
	}
	
	@RequestMapping(value="/logout",method = RequestMethod.GET)
    public String logout(HttpServletRequest request,HttpServletResponse response,Model model){
		HttpSession session = request.getSession();
		session.invalidate();
		model.addAttribute("logoutMsg", "You have been successfully logged out;");
		model.addAttribute("user", new Users());
		return "Login";
    }	
	
	//Show Login Page
	@RequestMapping(value = "loginPage", method = RequestMethod.GET)
	public String showLogin(Model model) {
		model.addAttribute("user", new Users());
		return "Login";
    }	
	
}
