<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>

<html>
	<head>
		<title>Faculty</title>
		<jsp:include page="fragments/facultyheader.jsp" />
		<link href="<c:url value="/resources/core/css/bootstrap.min.css"/>" rel="stylesheet" />
	</head>
<body>
<div class="container">
	<div class="row">
		
        
        <div class="col-md-12">
        <table width="100%">
			<tr>
				<td><h4>Faculty</h4></td>
				<td style="text-align: right;"><a href="<c:url value='/add_faculty'/>" class="btn btn-primary">ADD</a></td>
		    </tr>
    	</table>
       <c:if test="${!empty listFaculties}">
        <div class="table-responsive">
        	
              <table id="mytable" class="table table-bordred table-striped">
                    <tr>
	                   	<th>ID</th>
	                   	<th>Full Name</th>
		                <th>Mobile</th>
		                <th>Email</th>
		                <th>Show</th>
		                <th>Edit</th>
		                <th>Delete</th>
                    </tr>
    					<c:forEach items="${listFaculties}" var="faculty">
			                <tr>
			                	<td>${faculty.faculty_id}</td>
			                	<td>${faculty.faculty_abbreviation} ${faculty.faculty_firstname} ${faculty.faculty_middlename} ${faculty.faculty_lastname}</td>
                    			<td>${faculty.faculty_mobile}</td>
			         			<td>${faculty.faculty_email}</td>
         						<td><p data-placement="top" data-toggle="tooltip" title="Show">
			                    <a class="btn btn-primary btn-xs" href="<c:url value='/showFaculty/${faculty.faculty_id}'/>" ><span class="glyphicon glyphicon-plus-sign"></span></a></p></td>
			                    <td><p data-placement="top" data-toggle="tooltip" title="Edit">
			                    <a class="btn btn-primary btn-xs" href="<c:url value='/editFaculty/${faculty.faculty_id}'/>" ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
			                    <td><p data-placement="top" data-toggle="tooltip" title="Delete">
			                    <a class="btn btn-danger btn-xs" href="<c:url value='/removeFaculty/${faculty.faculty_id}'/>" ><span class="glyphicon glyphicon-trash"></span></a></p></td>
			                </tr>
            			</c:forEach>
				    </tbody>
				</table>   
			  
          </div>
          <div style="text-align: center;">
	          <ul class="paging">
		        <c:if test="${page > 1}">
		            <li><a href="<c:url value="/faculties"><c:param name="page" value="${page - 1}"/>${page - 1}</c:url>"><</a></li>
		        </c:if>
	        	<li><spring:message text="${page}"/></li>
		        <c:if test="${listFaculties.size() == 10}">
		            <li><a href="<c:url value="/faculties"><c:param name="page" value="${page + 1}"/>${page + 1}</c:url>">></a></li>
		        </c:if>
	    	  </ul>
    	  </div>
          </c:if>    
        </div>
	</div>
</div>
</body>
</html>	
