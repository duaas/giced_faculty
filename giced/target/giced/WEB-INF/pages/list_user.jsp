<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Users</title>
<jsp:include page="fragments/facultyheader.jsp" />
<link href="<c:url value="/resources/core/css/bootstrap.min.css"/>" rel=stylesheet type="text/css" />
</head>
<body>
<div class="container">
	<div class="row">
		
        
        <div class="col-md-12">
        <table width="100%">
			<tr>
				<td><h4>Users</h4></td>
				<td style="text-align: right;"><a href="<c:url value='/add_user'/>" class="btn btn-primary">ADD</a></td>
		    </tr>
    	</table>
       <c:if test="${!empty listUser}">
        <div class="table-responsive">
        	
              <table id="mytable" class="table table-bordred table-striped">
                    <tr>
	                   	<th>ID</th>
	                	<th>Username</th>
		                <th>Role</th>
		                <th>Edit</th>
		                <th>Delete</th>
                    </tr>
    					<c:forEach items="${listUser}" var="user">
			                <tr>
			                	<td>${user.user_id}</td>
			                	<td>${user.user_name}</td>
			                    <td>
				                    <c:forEach items="${listRole}" var="role">
				                    <c:if test = "${user.role_id == role.role_id}">
				           				 ${role.role_name}
				         			</c:if>
				         			</c:forEach>
			         			</td>
			                    <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a class="btn btn-primary btn-xs" href="<c:url value='/editUser/${user.user_id}'/>" ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
			                    <td><p data-placement="top" data-toggle="tooltip" title="Delete"><a class="btn btn-danger btn-xs" href="<c:url value='/removeUser/${user.user_id}'/>" ><span class="glyphicon glyphicon-trash"></span></a></p></td>
			                </tr>
            			</c:forEach>
				    </tbody>
				</table>   
			  
          </div>
          <div style="text-align: center;">
	          <ul class="paging">
		        <c:if test="${page > 1}">
		            <li><a href="<c:url value="/users"><c:param name="page" value="${page - 1}"/>${page - 1}</c:url>"><</a></li>
		        </c:if>
	        	<li><spring:message text="${page}"/></li>
		        <c:if test="${listUser.size() == 10}">
		            <li><a href="<c:url value="/users"><c:param name="page" value="${page + 1}"/>${page + 1}</c:url>">></a></li>
		        </c:if>
	    	  </ul>
    	  </div>
        </c:if>      
        </div>
	</div>
</div>
</body>
</html>